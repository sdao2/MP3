//
//  server.c
//  
//
//  Created by Stefan Dao on 9/1/15.
//
//

#define _XOPEN_SOURCE 700
#define _BSD_SOURCE

#include "server.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>

typedef struct _membershipList{
    unsigned int numberOfServers;
    unsigned int ** listInfo;
} membershipList;

typedef struct _fileList{
    unsigned int numberOfFiles;
    //unsigned int * location;
    char ** files;
    
} fileList;

unsigned int tfail = 2;
unsigned int tcleanup = 4;


FILE * fptr;
unsigned int selfID;
char * ip;
int introducerfd;

char * introducerip = "172.22.150.218";
char * leaderip = NULL;
//unsigned int leaderid = 2001;

fileList * fileLists;
membershipList * list;
unsigned int globalCount;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t fileLock = PTHREAD_MUTEX_INITIALIZER;
char filename[56];

int leaveGroup;
int checkFiles;

void printMembershipList(){
    pthread_mutex_lock(&m);
    for(int i = 0; i < list->numberOfServers; i++)
    {
        printf("MachineID: %u, HeartBeat: %u, Time: %u\n", *list->listInfo[i], *(list->listInfo[i]+1), *(list->listInfo[i]+2));
    }
    
    pthread_mutex_unlock(&m);
}

void buildMembershipList(unsigned int * buffer){
    
    pthread_mutex_lock(&m);
    if(leaveGroup == 1){
        pthread_mutex_unlock(&m);
        return;
    }
    
    unsigned int numberOfServers = *buffer;
    unsigned int currentCount = list->numberOfServers;
    //printf("Current count = %d\n", currentCount);
    unsigned int listSize = currentCount;
    
    buffer++;
    int failedDetected = 0;
    for(int i = 0; i < numberOfServers; i++)
    {
        unsigned int ID = *buffer;
        buffer++;
        unsigned int count = *buffer;
        buffer++;
        unsigned int time = *buffer;
        buffer++;
        
        int exists = 0;
        unsigned int * node;
        for(int j = 0; j < list->numberOfServers; j++)
        {
            //printf("ID1 = %u Beats = %u Count = %u\n", ID,list->numberOfServers, time);
            //printf("ID2 = %u Beats = %u Count = %u\n", *list->listInfo[j],*(list->listInfo[j]+1), *(list->listInfo[j]+2));
            if((list->listInfo[j] != NULL))
            {
                node = list->listInfo[j];
                if(ID == *list->listInfo[j])
                {
                    exists = 1;
                    
                    if(count == 0)
                    {
                        fptr = fopen(filename, "a");
                        fprintf(fptr, "Machine with ID = %u is LEAVING group at GlobalCount of %u\n", *node, globalCount);
                        fclose(fptr);
                        free(node);
                        node = NULL;
                        list->listInfo[j] = NULL;
                        failedDetected = 1;
                        currentCount--;
                        
                    }
                    
                    else if(count > *(node+1))
                    {
                        *(node + 1) = count;
                        *(node + 2) = globalCount;
                    }
                    
                    
                }
                
                else
                {
                    //printf("globalCount = %u, count = %u, node = %u\n", globalCount, count, *(node +2));
                    if(globalCount - *(node +2) > tcleanup)
                    {
                        fptr = fopen(filename, "a");
                        fprintf(fptr, "Failure detected on machine with ID = %u at GlobalCount of %u\n", *node, globalCount);
                        fclose(fptr);
                        free(node);
                        node = NULL;
                        list->listInfo[j] = NULL;
                        failedDetected = 1;
                        checkFiles = 1;
                        currentCount--;
                        
                        
                    }
                }
            }
        }
        

        
        if(exists != 1)
        {
            if(count != 0){
                unsigned int ** templist = malloc((currentCount + 1)*sizeof(unsigned int *));
                for(int j = 0; j < listSize; j++)
                {
                    templist[j] = list->listInfo[j];
                }
                
                
                //realloc(list->listInfo, (currentCount + 1)*sizeof(unsigned int *));
                unsigned int * temp = malloc(3*sizeof(unsigned int));
                
                list->listInfo = templist;
                list->listInfo[currentCount] = temp;
                *temp = ID;
                temp++;
                *temp = count;
                temp++;
                *temp = globalCount;
                listSize++;
                currentCount++;
                
                fptr = fopen(filename, "a");
                fprintf(fptr, "New machine with ID: %u joined at GlobalCount %u\n", ID, globalCount);
                fclose(fptr);
            }
        }
    }
    
    if(failedDetected == 1)
    {
        unsigned int ** newList = malloc(sizeof(unsigned int *) * currentCount);
        printf("List size = %u Current count failure = %u\n",listSize, currentCount);
        
        unsigned int ** ptr = newList;
        for(int i = 0; i < listSize; i++)
        {
            if(list->listInfo[i] != NULL)
            {
                printf("i = %d\n", i);
                *newList = list->listInfo[i];
                newList++;
            }
            else
            {
                printf("Null node detected\n");
            }
        }
        free(list->listInfo);
        list->listInfo = ptr;
    }
    list->numberOfServers = currentCount;
    
    pthread_mutex_unlock(&m);
}

void * buildMembershipListSerialized(){
    
    pthread_mutex_lock(&m);
    if(leaveGroup == 1){
        pthread_mutex_unlock(&m);
        return NULL;
    }
    
    unsigned int * buffer = malloc(sizeof(unsigned int) + list->numberOfServers*(sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int)));
    memset(buffer, '\0', sizeof(unsigned int) + list->numberOfServers*(sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int)));
    
    void * ptr = buffer;
    *buffer = list->numberOfServers;
    buffer++;
    unsigned int numberOfGoodServers = 0;
    for(int i = 0; i < list->numberOfServers; i++)
    {
        //if(!(*(list->listInfo[i]+1) - globalCount > tfail))
        if( (globalCount - *(list->listInfo[i]+2)) < tfail )
        {
            //memcpy(buffer, list->listInfo[i], 3*sizeof(unsigned int));
        
            *buffer = *list->listInfo[i];
            *(buffer+1) = *(list->listInfo[i] + 1);
            *(buffer+2) = *(list->listInfo[i] + 2);
            buffer += 3;
            numberOfGoodServers++;
        }
        else{
            printf("i = %d\n", i);
            
        }
    }
    //realloc(buffer, sizeof(unsigned int) + numberOfGoodServers*(sizeof(unsigned int) + sizeof(unsigned int) + sizeof(unsigned int)));

    
    unsigned int * point = (unsigned int *)ptr;
    *point = numberOfGoodServers;
    
    pthread_mutex_unlock(&m);
    return ptr;
}

int connectToServer(const char * ip)
{
    int s, fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_STREAM, 0);
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    s = getaddrinfo(ip, "1024", &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        free(result);
        return -1;
    }
    
    if(connect(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        free(result);
        perror("connect()");
        return -1;
    }
    else
    {
        free(result);
        printf("TCP Connection on client made with ip %s!\n", ip);
    }
    
    return fd;
}

void getIP(){
    //Gets IP of server
    struct ifaddrs * addrs;
    getifaddrs(&addrs);
    struct ifaddrs * tmp = addrs;
    
    while (tmp)
    {
        if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_INET)
        {
            struct sockaddr_in *pAddr = (struct sockaddr_in *)tmp->ifa_addr;
            if(strcmp(tmp->ifa_name, "eno") >= 0 )
            {
                ip = malloc(strlen(inet_ntoa(pAddr->sin_addr)) + 1);
                memset(ip, '\0', strlen(inet_ntoa(pAddr->sin_addr) + 1));
                strcpy(ip,inet_ntoa(pAddr->sin_addr));
                //printf("%s: %s\n", tmp->ifa_name, ip);
                printf("IP = %s\n", ip);
            }
        }
        
        tmp = tmp->ifa_next;
    }
    freeifaddrs(addrs);
    
}

int createTCPSocketConnection(char * port){
    int fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_STREAM, 0);
    int optval = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    int s = getaddrinfo(NULL, port, &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }
    
    if(bind(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        perror("bind()");
        return -1;
    }
    
    //Set this number depending on VM cluster size
    int number_of_machines = 7;
    
    if(listen(fd, number_of_machines) != 0)
    {
        perror("listen()");
        return -1;
    }
    
    struct sockaddr_in *result_addr = (struct sockaddr_in *) result->ai_addr;
    printf("Listening on file descriptor %d, port %d\n", fd, ntohs(result_addr->sin_port));
    
    return fd;
}


int UDPConnect(const char * ip, const char * port){
    int s, fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    
    s = getaddrinfo(ip, port, &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        free(result);
        return -1;
    }
    
    if(connect(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        free(result);
        perror("connect()");
        return -1;
    }
    else
    {
        free(result);
        //printf("UDP Connection on client made with ip %s on port %s!\n", ip, port);
    }
    
    return fd;
}

int createUDPSocketConnection(char * udpport){
    int fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    int optval = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(optval));
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    
    int s = getaddrinfo(NULL, udpport, &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }
    
    if(bind(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        perror("bind()");
        return -1;
    }
    
    return fd;
}

int isMemberOfGroup(int id)
{
    
    pthread_mutex_lock(&m);
    for(int i = 0; i < list->numberOfServers; i++)
    {
        if(*(list->listInfo[i]) == id)
        {
            pthread_mutex_unlock(&m);
            return 1;
        }
    }
    pthread_mutex_unlock(&m);
    return 0;
}

int isFilePresent(char * filename)
{
    for(int i = 0; i < fileLists->numberOfFiles; i++)
    {
        if (strstr(fileLists->files[i], filename) != NULL)
            return 1;
    }
    return 0;
}

int getPredecessor()
{
    int ID = selfID + 1;
    
    while(selfID != ID && !isMemberOfGroup(ID))
    {
        if(ID > 224)
            ID = 218;
        else
            ID++;
    }
    
    return ID;
}

int getSuccessor()
{
    int ID = selfID - 1;
    
    while(selfID != ID && !isMemberOfGroup(ID))
    {
        if(ID < 218)
            ID = 224;
        else
            ID--;
    }
    
    return ID;
}

void duplicateFiles(){
    while(1){
        sleep(1);
        
        if(checkFiles){
            //pthread_mutex_lock(&fileLock); //Mutex lock - Critical Section
            
            for(int i = 0; i < fileLists->numberOfFiles; i++)
            {
                char * file = fileLists->files[i];
                int replies = 0;
                
                for(int j = 0; j < list->numberOfServers; j++)
                {
                    char * buffer = malloc(16); //Adjust to IP
                    memset(buffer, '\0', 16);
                    
                    if(j < list->numberOfServers){
                        sprintf(buffer, "172.22.150.%u", *list->listInfo[j]);
                    }
                    
                    int fd = connectToServer(buffer);
                    
                    
                    
                    char * input = malloc(256);
                    memset(input, '\0', 256);
                    sprintf(input, "list %s", file);
                    
                    write(fd, input, 256);
                    
                    char output[256];
                    memset(output, '\0', 256);
                    
                    if(read(fd, output, 256) > 0)
                    {
                        unsigned int replyingServer = *(unsigned int *)output;
                        printf("Got reply from replyingServer = %u\n", replyingServer);
                        if(replyingServer == getSuccessor())
                        {
                            printf("ReplyingServer == Successor\n");
                            replies = 3;
                            checkFiles = 0;
                            close(fd);
                            break;
                        }
                        else
                        {
                            replies++;
                        }
                        memset(output, '\0', 256);
                    }
                    close(fd);
                }
                printf("We're out\n");
                if(replies < 3)
                {
                    printf("Got less than 3 replies\n");
                    
                    char forwardedOutput[256];
                    char * temp = forwardedOutput;
                    memset(forwardedOutput, '\0', 256);
                    strcpy(temp, "put ");
                    temp = temp + 4;
                    strcpy(temp, "DNF ");
                    temp = temp + 4;
                    strcpy(temp, file);
                    
                    if(replies <= 2){
                        int successorid = getSuccessor();
                        char * successorip = malloc(16);
                        sprintf(successorip, "172.22.150.%u", successorid);
                        
                        int successorfd = connectToServer(successorip);
                        write(successorfd, forwardedOutput, 256);
                        
                        FILE * fpserver = fopen(file, "r");
                        
                        char  *output2 = malloc(256);
                        int count;
                        size_t length = 0;
                        printf("Forwarding files\n");
                        while((count = getline(&output2, &length, fpserver)) > 0)
                        {
                            
                            write(successorfd, output2, count);
                        }
                        fclose(fpserver);
                        free(output2);
                        
                        close(successorfd);
                        free(successorip);
                    }
                    if(replies <= 1)
                    {
                        int predecessorid = getPredecessor();
                        char * predecessorip = malloc(16);
                        sprintf(predecessorip, "172.22.150.%u", predecessorid);
                        
                        int predecessorfd = connectToServer(predecessorip);
                        write(predecessorfd, forwardedOutput, 256);
                        
                        FILE * fpserver = fopen(file, "r");
                        
                        char  *output2 = malloc(256);
                        int count;
                        size_t length = 0;
                        printf("Forwarding files\n");
                        while((count = getline(&output2, &length, fpserver)) > 0)
                        {
                            
                            write(predecessorfd, output2, count);
                        }
                        fclose(fpserver);
                        free(output2);
                        
                        close(predecessorfd);
                        free(predecessorip);
                        
                    }
                    
                    
                    
                    
                }
                else
                {
                    checkFiles = 0;
                }
            }
            
            //pthread_mutex_unlock(&fileLock); //Mutex unlock
        }
    }
}

void acceptTCPConnections(int fd){
    
    int clientfd;
    while((clientfd = accept(fd, NULL, NULL)) != 0)
    {
        printf("Connection made on server!\n");
        char buffer[256];
        memset(buffer, '\0', 256);
        int len = read(clientfd, buffer, 256);
        
        char buffercp[256];
        memcpy(buffercp, buffer, 256);
        printf("Message = \"%s\"\n", buffer);
        
        //Opens command in new process and pipes output to fp
        //Send output back to client for printing
        if(strstr(buffer, "grep") != NULL){
            FILE * fp = popen(buffer, "r");
            if(fp == NULL)
            {
                printf("Command failed!\n");
            }
            char  *output = malloc(256);
            
            int count;
            size_t length = 0;
            
            while((count = getline(&output, &length, fp)) > 0)
            {
                write(clientfd, output, count);
            }
            
            free(output);
        }
        
        else if(strstr(buffer, "put") != NULL){
            char * pch;
            pch = strtok (buffer," ");
            
            char * lfs = strtok(NULL," ");
            char * dfs = strtok(NULL," ");
            
            printf("put = %s, 2 = %s, 3 = %s, %lu\n", pch, lfs, dfs, strlen(dfs));
            
            char output[256];
            memset(output, '\0', 256);
            
            FILE * fp = fopen(dfs, "w");
            
            while(read(clientfd, output, 256) > 0)
            {
                //printf("Output to server is: %s\n", output);
                fprintf(fp, "%s", output);
                memset(output, '\0', 256);
            }
            
            fclose(fp);
            
            pthread_mutex_lock(&fileLock); //Mutex lock - Critical Section
            
            char ** templist = malloc(sizeof(char *)*(fileLists->numberOfFiles+1));
            for(int i = 0; i < fileLists->numberOfFiles; i++)
            {
                templist[i] = fileLists->files[i];
            }
            
            char * dfscpy = malloc(strlen(dfs) + 1);
            dfscpy[sizeof(dfs)] = '\0';
            strcpy(dfscpy, dfs);
            templist[fileLists->numberOfFiles] = dfscpy;
            
            free(fileLists->files);
            fileLists->files = templist;
            fileLists->numberOfFiles++;
            
            pthread_mutex_unlock(&fileLock); //Mutex unlock
            
            if(strstr(lfs, "DNF") == NULL) {
                int successorID = getSuccessor();
                int predecessorID = getPredecessor();
                char * successorip = malloc(16);
                sprintf(successorip, "172.22.150.%u", successorID);
                char * predecessorip = malloc(16);
                sprintf(predecessorip, "172.22.150.%u", predecessorID);
                
                int successorfd = connectToServer(successorip);
                int predecessorfd = connectToServer(predecessorip);
                
                int arr[2] = {successorfd, predecessorfd};
                
                char forwardedOutput[256];
                char * temp = forwardedOutput;
                memset(forwardedOutput, '\0', 256);
                strcpy(temp, "put ");
                temp = temp + 4;
                strcpy(temp, "DNF ");
                temp = temp + 4;
                strcpy(temp, dfs);
                
                
                for( int i = 0; i < 2; i++)
                {
                    
                    write(arr[i], forwardedOutput, 256);
                    
                    FILE * fpserver = fopen(dfs, "r");
                    
                    char  *output2 = malloc(256);
                    int count;
                    size_t length = 0;
                    printf("Forwarding files\n");
                    while((count = getline(&output2, &length, fpserver)) > 0)
                    {
                        
                        write(arr[i], output2, count);
                    }
                    fclose(fpserver);
                    free(output2);
                }
                
                free(successorip);
                free(predecessorip);
                close(successorfd);
                close(predecessorfd);
                
            }
            printf("Number of files on server = %d\n", fileLists->numberOfFiles);
        }
        else if(strstr(buffer, "get") != NULL)
        {
            
            char * pch;
            pch = strtok (buffer," ");
            
            char * lfs = strtok(NULL," ");
            char * dfs = strtok(NULL," ");
            
            printf("get = %s, 2 = %s, 3 = %s, %lu\n", pch, lfs, dfs, strlen(dfs));
            FILE * fp = fopen(lfs, "r");
            
            if(fp == NULL)
                printf("Could not open file!\n");
            
            char  *output = malloc(256);
            
            int count;
            size_t length = 0;
            
            while((count = getline(&output, &length, fp)) > 0)
            {
                write(clientfd, output, count);
            }
            
            free(output);
            fclose(fp);
            
        }
        
        else if(strstr(buffer, "store") != NULL)
        {
            for (int i = 0; i < fileLists->numberOfFiles; i++)
            {
                char * character = malloc(1);
                *character = ' ';
                printf("i = %d w/ %s\n", i, fileLists->files[i]);
                write(clientfd, fileLists->files[i], strlen(fileLists->files[i]));
                write(clientfd, character, 1);
                free(character);
            }
        }
        
        else if(strstr(buffer, "list") != NULL)
        {
            char * pch;
            pch = strtok (buffer," ");
            
            char * searchedFile = strtok(NULL," ");
            
            unsigned int * tempint = malloc(sizeof(unsigned int));
            *tempint = selfID;
            
            for (int i = 0; i < fileLists->numberOfFiles; i++)
            {
                
               if(strstr(fileLists->files[i], searchedFile) != NULL)
               {
                   write(clientfd, tempint, sizeof(unsigned int));
                   printf("Wrote something out\n");
                   break;
               }
            }
            free(tempint);
        }
        else if(strstr(buffer, "delete") != NULL)
        {
            char * pch;
            pch = strtok (buffer," ");
            char * deletedFile = strtok(NULL," ");
            
            int didNotFindFile = 1;
            
            pthread_mutex_lock(&fileLock);
            for (int i = 0; i < fileLists->numberOfFiles; i++)
            {
                
                if(strstr(fileLists->files[i],deletedFile) != NULL)
                {
                    free(fileLists->files[i]);
                    fileLists->files[i] = NULL;
                    didNotFindFile = 0;
                    break;
                }
            }
            
            if(!didNotFindFile){
            
                char ** tempList = malloc((fileLists->numberOfFiles - 1)*sizeof(char *));
                int x = 0;
                for(int i = 0 ; i < fileLists->numberOfFiles; i++)
                {
                    if(fileLists->files[i] != NULL)
                    {
                        tempList[x++] = fileLists->files[i];
                    }
                }
                free(fileLists->files);
                fileLists->files = tempList;
                fileLists->numberOfFiles--;
            }
            else
            {
                printf("Was not able to delete file! Not found!\n");
            }
            pthread_mutex_unlock(&fileLock); //Mutex unlock
        }
        
        close(clientfd);
    }
    
}

void acceptUDPConnections(int udpfd){
    
    int received;
    char * buf = malloc(2048);
    memset(buf, '\0', 2048);
    
    struct sockaddr_storage remaddr;
    memset(&remaddr, 0, sizeof(struct sockaddr_storage));
    socklen_t addrlen = 0;
    
    while(1){
        if((received = recvfrom(udpfd, buf, 2048, MSG_DONTWAIT, (struct sockaddr *)&remaddr, &addrlen)) > 0)
        {
            buf[received] = '\0';
            //printf("Received message\n");
            unsigned int * buffer = (unsigned int *)buf;
            buildMembershipList(buffer);
            
            memset(buf, '\0', 2048);
        }
        
    }
}

void sendUDPMessages(const char * port){
    
    while(1){

        sleep(1);
        globalCount++;
        
        if(leaveGroup == 1)
        {
            pthread_mutex_lock(&m);
            for(int i = 0; i < list->numberOfServers; i++)
            {
                char * buffer = malloc(16); //Adjust to IP
                memset(buffer, '\0', 16);
                
                unsigned int * ptr = malloc(4*sizeof(unsigned int));
                *ptr = 1;
                *(ptr + 1) = selfID;
                *(ptr + 2) = 0;
                *(ptr + 3) = 0;
                sprintf(buffer, "172.22.150.%u", *list->listInfo[i]);
                int udpClientFd = UDPConnect(buffer, "1030");
                write(udpClientFd, ptr, 4*sizeof(unsigned int));
                
                free(buffer);
                free(list->listInfo[i]);
                list->listInfo[i] = NULL;
            }
            
            free(list);
            
            list = malloc(sizeof(membershipList));
            list->numberOfServers = 1;
            list->listInfo = malloc(sizeof(unsigned int *));
            list->listInfo[0] = malloc(sizeof(unsigned int) * 3);
            
            *(list->listInfo[0]) = selfID;
            *(list->listInfo[0] +1) = 0;
            *(list->listInfo[0] +2) = 0;
            
            pthread_mutex_unlock(&m);
        }
        
        for(int i = 0; i < list->numberOfServers; i++)
        {
            if(*list->listInfo[i] == selfID)
            {
                
                *(list->listInfo[i]+1) = globalCount;
                *(list->listInfo[i]+2) = globalCount;
            }
        }
        
        for(int i = 0; i < list->numberOfServers; i++)
        {
            char * buffer = malloc(15); //Adjust to IP
            memset(buffer, '\0', 15);
            void * ptr = buildMembershipListSerialized();
            
            if(ptr == NULL)
                continue;
            
            pthread_mutex_lock(&m);
            if(i < list->numberOfServers){
                sprintf(buffer, "172.22.150.%u", *list->listInfo[i]);
                //printf("Sending membership list\n");
            
            
                int udpClientFd = UDPConnect(buffer, "1030");
                write(udpClientFd, ptr, sizeof(unsigned int) + list->numberOfServers*(sizeof(unsigned int)*3));
                
                
                close(udpClientFd);
            }
            pthread_mutex_unlock(&m);
            free(ptr);
        }
        
    }
}

void acceptTCPServerConnections(int tcpfd){
    
    int clientfd;
    
    while((clientfd = accept(tcpfd, NULL, NULL)) != 0)
    {
        printf("Connection made on server-server!\n");
        char buffer[257];
        int len = read(clientfd, buffer, 256);
        buffer[len-1] = '\0';
        printf("IP Joined = \"%s\"\n", buffer);
        
        unsigned int id = strtoul(buffer, NULL, 10);
        
        printf("Id of joined is %u l\n", id);
        unsigned int * newID = malloc(sizeof(unsigned int)*4);
        
        *newID = 1;
        *(newID +1) = id;
        *(newID +2) = 0;
        *(newID +3) = 0;
        buildMembershipList(newID);
        void * mList = buildMembershipListSerialized();
        
        if(mList == NULL) continue;
        
        unsigned int numberOfGoodServers = *((int *)mList);
        
        write(clientfd, mList, sizeof(unsigned int)+ numberOfGoodServers*(sizeof(unsigned int)*3));

        free(mList);
        
        close(clientfd);
    }
}

int sendTCPForJoin(char * ip){
    int s, fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_STREAM, 0);
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    s = getaddrinfo(introducerip, "2048", &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        free(result);
        return -1;
    }
    
    if(connect(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        free(result);
        perror("connect()");
        return -1;
    }

    free(result);
    printf("TCP Connection on client made with ip on port 2048 %s!\n", ip);

    char * temp = malloc(strlen(ip) + 1);
    memset(temp, '\0', strlen(ip) + 1);
    memcpy(temp, ip, strlen(ip));
    
    write(fd, temp, strlen(temp) +1);

    char * output = malloc(256);
    memset(output, '\0', 256);
    
    free(temp);
    while(read(fd, output, 256) > 0)
    {
        printf("Output = %u\n", *output);
    }
    unsigned int * ptr = (unsigned int *)output;
    
    buildMembershipList((unsigned int *)output);
    free(output);
    
    return fd;
}

void * acceptTCPForJoin(void * ptr){
    int tcpfd = createTCPSocketConnection("2048");
    acceptTCPServerConnections(tcpfd);
    
    return NULL;
}

void * acceptTCPConnectionsPT(void * ptr)
{
    acceptTCPConnections(*((int *)ptr));
    return NULL;
}

void * acceptUDPConnectionsPT(void * ptr)
{
    acceptUDPConnections(*((int *)ptr));
    return NULL;
}

void * sendUDPMessagesPT(void * ptr)
{
    sendUDPMessages((char *)ptr);
    return NULL;
}

void * duplicateFilesPT(void * ptr){
    duplicateFiles();
    return NULL;
}

void joinGroup(){
    leaveGroup = 0;
    
    if(strcmp(introducerip, ip) == 0)
    {
        
        //char * ipList[7] = {"172.22.150.218", "172.22.150.219", "172.22.150.220", "172.22.150.221", "172.22.150.222", "172.22.150.223", "172.22.150.224"};
        //int numberIPs = 3;
        //char * ipList[3] = {"2002", "2003", "2004"};
        char * ipList[6] = { "172.22.150.219", "172.22.150.220", "172.22.150.221", "172.22.150.222", "172.22.150.223", "172.22.150.224"};
        for(int i = 0; i < 6; i++)
        {
            char * buffer = malloc(15); //Adjust to IP
            memset(buffer, '\0', 15);
            
            unsigned int * ptr = malloc(4*sizeof(unsigned int));
            *ptr = 1;
            *(ptr + 1) = selfID;
            *(ptr + 2) = globalCount;
            *(ptr + 3) = globalCount;
            
            sprintf(buffer, "%s", ipList[i]);
            int udpClientFd = UDPConnect(buffer, "1030");
            write(udpClientFd, ptr, 4*sizeof(unsigned int));
            
            fptr = fopen(filename, "a");
            fprintf(fptr, "Machine with ID = %u has joined\n", selfID);
            fclose(fptr);
            free(buffer);
        }
        
    }
    else
    {
        int result = sendTCPForJoin(ip);
        if(result == -1)
            printf("Leader is down\n");
        else{
            fptr = fopen(filename, "a");
            fprintf(fptr, "Machine with ID = %u has joined\n", selfID);
            fclose(fptr);
        }
    }
}

void leaveGroupGracefully(){
    leaveGroup = 1;
    
    fptr = fopen(filename, "a");
    fprintf(fptr, "Machine with ID = %u has gracefully left\n", selfID);
    fclose(fptr);
}

void * acceptUserCommandsPT(void * ptr){
    printf("Type number for command.\n");
    printf("1. List membership list\n");
    printf("2. List ID\n");
    printf("3. Join the group\n");
    printf("4. Leave the group\n");
    printf("5. Print successor and predecessor\n");
    while(1){
        char * buffer = malloc(2);
        memset(buffer, '\0', 2);
        fgets(buffer, 256, stdin);
        
        switch(*buffer){
            case '1': printMembershipList();
                break;
            case '2': printf("Machine ID: %u\n", selfID);
                break;
            case '3': joinGroup();
                break;
            case '4': leaveGroupGracefully();
                break;
            case '5': printf("SuccessorID: %d, PredecessorID: %d\n", getSuccessor(), getPredecessor());
                
                
        }
        
        free(buffer);
    }
    return NULL;
}

int main(int argc, char ** argv)
{
    //TCP Client Connection - Port 1024
    //TCP Server Connection - Port 2048
    //UDP Receive Connection - Port 1030 or argv[1]
    //UDP Send Connection - Port 1030 argv[2]
    //Master TCP Commands - Port 2000
    
    globalCount = 0;
    leaveGroup = 0;
    checkFiles = 0;
    int status, leader = 0, result;
    
    pthread_t id0, id1, id2, id3, id4, id5;
    pid_t child0;

    getIP();
    
    //Initialize membershipList
    
    list = malloc(sizeof(membershipList));
    list->numberOfServers = 1;
    list->listInfo = malloc(sizeof(unsigned int *));
    list->listInfo[0] = malloc(sizeof(unsigned int) * 3);
    
    //Initialize fileLists
    
    fileLists = malloc(sizeof(fileList));
    fileLists->files = NULL;
    fileLists->numberOfFiles = 0;
    
    char * idgrab = malloc(4);
    char * throwaway;
    
    memcpy(idgrab, ip + (strlen(ip) -3), 3);
    unsigned int id = strtoul(idgrab, &throwaway, 10); //Change to IP //Local
    free(idgrab);
    
    //Get ID based on IP
    
    selfID = id;
    *(list->listInfo[0]) = id;
    *(list->listInfo[0] +1) = 0;
    *(list->listInfo[0] +2) = 0;
    
    sprintf(filename,"log%u.txt", id);
    fptr = fopen(filename, "w");
    fclose(fptr);
    
    if(strcmp(ip, "172.22.150.218") == 0){
        pthread_create(&id0, NULL, acceptTCPForJoin, NULL);
    }
    
    else
    {
        //result = sendTCPForJoin(port);
             
        result = sendTCPForJoin(ip);
        if(result == -1)
        {
            printf("Leader is down\n");
            free(list->listInfo[0]);
            free(list->listInfo);
            free(list);
            free(ip);
            return 0;
        }
    }
    
    fptr = fopen(filename, "a");
    fprintf(fptr, "Machine with ID = %u has joined\n", selfID);
    fclose(fptr);
    
    int tcpfd = createTCPSocketConnection("1024");
    int udpfd = createUDPSocketConnection("1030");
    //int mastertcpfd = createTCPSocketConnection("2000");
    

    int * tcp = malloc(sizeof(int));
    int * udp = malloc(sizeof(int));
    char * udpsend = malloc(sizeof(char)*4);
    *tcp = tcpfd;
    *udp = udpfd;
    memcpy(udpsend, "1030", 4);

    pthread_create(&id1, NULL, acceptTCPConnectionsPT, (void *)tcp);
    pthread_create(&id2, NULL, acceptUDPConnectionsPT, (void *)udp);
    pthread_create(&id3, NULL, sendUDPMessagesPT, (void *)udpsend);
    pthread_create(&id4, NULL, acceptUserCommandsPT, NULL);
    pthread_create(&id5, NULL, duplicateFilesPT, NULL);
    void * ptr;
    pthread_join(id0, &ptr);
    pthread_join(id1, &ptr);
    pthread_join(id2, &ptr);
    pthread_join(id3, &ptr);
    pthread_join(id4, &ptr);
    pthread_join(id5, &ptr);
    return 0;
}