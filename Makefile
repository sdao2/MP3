CC = gcc -std=c99
INC = -I.
FLAGS = -g -O2 -W -Wall
LINKOPTS = -lpthread -D_POSIX_SOURCE -std=c99

all: grep_server grep_client 

grep_server: server.o
	$(CC) $^ $(FLAGS) $(LINKOPTS) -g -o $@

server.o: server.c server.h
	$(CC) -o $@ $< -c -g $(OPTS) 

grep_client: client.o
	$(CC) $^ $(FLAGS) $(LINKOPTS) -g -o $@

client.o: client.c client.h
	$(CC) -o $@ $< -c $(OPTS) -g $(LINKOPTS)

.PHONY : clean
clean:
	-rm -f *.o *.so *.rp
