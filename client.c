//
//  client.c
//  
//
//  Created by Stefan Dao on 9/1/15.
//
//
//Users/Stefan/Desktop/CS425/mp2_local/client.c

#define _XOPEN_SOURCE 700
#define _BSD_SOURCE

#include "client.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>

int connectToServer(const char * ip)
{
    int s, fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_STREAM, 0);
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    
    s = getaddrinfo(ip, "1024", &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        free(result);
        return -1;
    }

    if(connect(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        free(result);
        perror("connect()");
        return -1;
    }
    else
    {
        free(result);
        printf("TCP Connection on client made with ip %s!\n", ip);
    }
    
    return fd;
}

char * getUserCommand()
{
    printf("Enter a user command: \n"); //Specific to MP1
    
    char * buffer = malloc(256);
    memset(buffer, '\0', 256);
    fgets(buffer, 256, stdin);
    
    return buffer;
}

void * threadedFunc(void * ptr)
{
    char * ip = (char *)ptr;
    
    return NULL;
}

int countFileLines(char * filename){
    int count = 0;
    FILE * fptr = fopen(filename, "r");
    while (!feof(fptr))
    {
        char ch = fgetc(fptr);
        if(ch == '\n')
            count++;
    }
    fclose(fptr);
    
    return count;
}

int computeHashFunction(char * filename, int num)
{
    int count = 0;
    for (int i = 0; i < strlen(filename); i++)
    {
        count += ((int)(*(filename+i)));
    }
    
    return count % num;
}

int getPredecessor(int hashNum, int numberOfServers)
{
    return hashNum - 1 < 0 ? numberOfServers - 1 : hashNum -1;
}

int getSuccessor(int hashNum, int numberOfServers)
{
    return (hashNum + 1) % numberOfServers;
}

int clientConnect(char * buffercp, char * filename)
{
    int numberOfTestedServers = 7;
    /*
     Insert IPs into ipList and edit array size accordingly. Use 127.0.0.1 to test on local machines
     */
    
    //char * ipList[1] = {"127.0.0.1"};
    char * ipList[7] = {"172.22.150.218", "172.22.150.219", "172.22.150.220", "172.22.150.221", "172.22.150.222", "172.22.150.223", "172.22.150.224"};
    
    int connections = 0;
    
    unsigned long numberOfIps = sizeof(ipList)/sizeof(char *);
    FILE * fptr;
    printf("buffer = %s\n", buffercp);
    buffercp[strlen(buffercp) - 1] = '\0';
    
    char * buffer = malloc(256);
    memset(buffer, '\0', 256);
    memcpy(buffer, buffercp, strlen(buffercp) + 1);
    
    char output[256];
    memset(output, '\0', 256);
    
    
    if(strstr(buffer, "grep") != NULL)
    {
        fptr = fopen(filename, "w");
        for(int i = 0; i < (int) numberOfIps; i++)
        {
   
            int fd = connectToServer(ipList[i]);
            if(fd < 0)
                continue;
            
            write(fd, buffer, strlen(buffer));
            
            while(read(fd, output, 256) > 0)
            {
                //printf("From IP (%s): %s", ipList[i], output);
                fprintf(fptr, "From IP (%s): %s", ipList[i], output);
                memset(output, '\0', 256);
            }
            
            connections++;
            close(fd);
        }
            fclose(fptr);
    }
    
    else if (strstr(buffer, "put") != NULL)
    {
        char * pch;
        pch = strtok (buffer," ");
        
        char * lfs = strtok(NULL," ");
        char * dfs = strtok(NULL," ");
        
        printf("put = %s, 2 = %s, 3 = %s, %s\n", pch, lfs, dfs, buffercp);
        
        int hash = computeHashFunction(dfs, numberOfTestedServers);
        int fd = connectToServer(ipList[hash]);
        printf("Hash number = %d\n", hash);
        write(fd, buffercp, 256);

        sleep(1);
        FILE * fp = fopen(lfs, "r");
        
        char  *output2 = malloc(256);
        int count;
        size_t length = 0;
        
        while((count = getline(&output2, &length, fp)) > 0)
        {
            write(fd, output2, count);
        }
        
        fclose(fp);
        free(output2);
        
    }
    else if (strstr(buffer, "get") != NULL)
    {
        char * pch;
        pch = strtok (buffer," ");
        
        char * lfs = strtok(NULL," ");
        char * dfs = strtok(NULL," ");
        
        printf("get = %s, 2 = %s, 3 = %s, %s\n", pch, lfs, dfs, buffercp);
        
        int hash = computeHashFunction(lfs, numberOfTestedServers);
        printf("Hash = %d\n", hash);
        
        FILE * ptr = fopen(dfs, "w");
        int successor = getSuccessor(hash, numberOfTestedServers);
        int predecessor = getPredecessor(hash, numberOfTestedServers);
        
        int i = 0;
        
        int fds[3] = {hash,successor,predecessor};
        
        while (i < 3)
        {
            
            int fd = connectToServer(ipList[fds[i]]);
            if(fd >= 0){
        
                write(fd, buffercp, 256);
                
                while(read(fd, output, 256) > 0)
                {
                    //printf("Output: %s",  output);
                    fprintf(ptr, "%s", output);
                    memset(output, '\0', 256);
                    i = 3;
                }
                close(fd);
            }
            i++;
            
        }
        
        if (i == 3)
        {
            printf("Could not find the file in servers!\n");
        }
        
        fclose(ptr);
        
    }
    else if (strstr(buffer, "delete") != NULL)
    {
        char * pch;
        pch = strtok (buffer," ");
        
        char * lfs = strtok(NULL," ");
        
        printf("delete = %s, 2 = %s, %s\n", pch, lfs, buffercp);
        
        for(int i = 0; i < numberOfIps; i++)
        {
            int fd = connectToServer(ipList[i]);
            if(fd < 0)
                continue;
            
            write(fd, buffercp, 256);
            close(fd);
        }
    }
    else if (strstr(buffer, "store") != NULL)
    {
        char * pch;
        pch = strtok (buffer," ");
        char * lfs = strtok(NULL," ");
        //172.22.150.218
        
        char * ip = malloc(15);
        sprintf(ip, "172.22.150.%s\0", lfs);
        printf("Ip = %s\n", ip);
        int fd = connectToServer(ip);
        int timesRead = 0;
        if(fd >= 0)
        {
            
            write(fd, buffercp, 256);
            //write(fd, buffer, strlen(buffer));
            
            while(read(fd, output, 256) > 0)
            {
                //printf("Times read = %d\n", ++timesRead);
                printf("Files on process %s: %s\n", lfs, output);
                //fprintf(fptr, "From IP (%s): %s", ipList[i], output);
                memset(output, '\0', 256);
            }
            close(fd);
        }
    }
    else if (strstr(buffer, "list"))
    {
        char * pch;
        pch = strtok (buffer," ");
        char * lfs = strtok(NULL," ");
        
        for(int i = 0; i < numberOfIps; i++)
        {
            int fd = connectToServer(ipList[i]);
            if(fd < 0)
                continue;
            write(fd, buffercp, 256);
            
            while(read(fd, output, 256) > 0)
            {
                printf("File %s on process : %u\n", lfs, *(unsigned int *)output);
                //fprintf(fptr, "From IP (%s): %s", ipList[i], output);
                memset(output, '\0', 256);
            }
            close(fd);
        }
        
    }
    
    free(buffer);
    free(buffercp);

    return connections;
    
}

int UDPConnect(const char * ip){
    int s, fd;
    struct addrinfo hints, *result;
    
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;
    
    s = getaddrinfo("127.0.0.1", "2048", &hints, &result);
    if(s != 0)
    {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        free(result);
        return -1;
    }
    
    if(connect(fd, result->ai_addr, result->ai_addrlen) != 0)
    {
        free(result);
        perror("connect()");
        return -1;
    }
    else
    {
        free(result);
        printf("UDP Connection on client made with ip %s!\n", ip);
    }
    
    return fd;
}

int runUnitTest(){
    //grep test test.txt
    
    char * buff = malloc(256);
    memset(buff, '\0', 256);
    strcpy(buff, "grep test test.txt\n");
    int connections = clientConnect(buff, "testlog.txt");
    
    
    int line_count = countFileLines("testlog.txt");
    assert(line_count == connections);
    printf("Testing testlog.txt file line count = %d\n", line_count );
    
    return 1;
    
}

int main()
{
    //Unit test code
    int success = 1; //runUnitTest();
    if(success)
        printf("Passed unit tests\n");
    
    char * buffer = getUserCommand();

    int line_count = clientConnect(buffer, "log.txt");;
    int status;
    
    printf("Returned from func\n");
    
    return 0;
}